import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {HotelValidators} from '../../validators/hotel.validators';

@Component({
  selector: 'app-hotel-form',
  templateUrl: './hotel-form.component.html',
  styleUrls: ['./hotel-form.component.css']
})
export class HotelFormComponent implements OnInit {

  Locations = [
    {
      id: 1,
      city: 'Colombo'
    },
    {
      id: 2,
      city: 'Kandy'
    },
    {
      id: 3,
      city: 'Mathara'
    }
  ];

  form = new FormGroup({
    name: new FormControl('', [Validators.required, HotelValidators.shouldBeUnique] ),
    email: new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
    location: new FormControl('', Validators.required),
    contactNumbers: new FormControl('', Validators.required)
  });

  constructor() { }

  ngOnInit(): void {
  }


  get name(): AbstractControl {
    return this.form.get('name');
  }

  get email(): AbstractControl {
    return this.form.get('email');
  }

  get location(): AbstractControl {
    return this.form.get('location');
  }

  get contactNumbers(): AbstractControl {
    return this.form.get('contactNumbers');
  }
}

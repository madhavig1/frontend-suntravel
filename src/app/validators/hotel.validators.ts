import {AbstractControl, ValidationErrors} from '@angular/forms';

export class HotelValidators{
  static  shouldBeUnique(control: AbstractControl): Promise<ValidationErrors | null> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (control.value === 'Cinna') {
          resolve({shouldBeUnique: true});
        }
        else {
          resolve(null);
        }
      }, 2000);
    });
  }
}
